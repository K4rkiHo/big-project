# PyGame Zombie Shooter
Jedná se o vlastní hru v Pythonu vytvořenou jako projekt do předmětu SKJ (skriptovací jazyky)
Hra je napsaná v Pythonu a využivá knihovnu **PyGame**.

# Instalation
Jediné co je zapotřebí nainstalovat je Python 3 a vyšší a PyGame.

# About PyGame
Pygame je využívám především při vykreslování a vstupů kláves pro pohyb postavičky po mapě.

# About Game
Jedná se o jednoduchou hru, kde je hlavním cíle střílení zombie, které na hráče nabíhají ve vlnách. Za každého mrtvého zombie je přičten hráčovi určitý počet bodů. Obtížnost hry se dá měnit.
